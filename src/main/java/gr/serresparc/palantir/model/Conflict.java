package gr.serresparc.palantir.model;

import java.util.Date;

public class Conflict
{


    private long id;
    private String location;
    private String embeddedHtml;
    private String typeOfConflict;
    private String source;
    private Double longitude;
    private Double latitude;
    private Date timeStamp;


    public Conflict(long id, String location, String typeOfConflict, String source, Double latitude,
                    Double longitude, Date timeStamp, String embeddedHtml)
    {
        this.id             = id;
        this.location       = location;
        this.typeOfConflict = typeOfConflict;
        this.source         = source;
        this.longitude      = longitude;
        this.latitude       = latitude;
        this.timeStamp      = timeStamp;
        this.embeddedHtml   = embeddedHtml;

    }

    public String getEmbeddedHtml()
    {
        return embeddedHtml;
    }

    public long getId()
    {
        return id;
    }

    public String getLocation()
    {
        return location;
    }

    public String getTypeOfConflict()
    {
        return typeOfConflict;
    }

    public String getSource()
    {
        return source;
    }

    public Double getLongitude()
    {
        return longitude;
    }

    public Double getLatitude()
    {
        return latitude;
    }

    public Date getTimeStamp()
    {
        return timeStamp;
    }
}
