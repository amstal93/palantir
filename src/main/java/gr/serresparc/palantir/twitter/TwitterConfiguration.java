package gr.serresparc.palantir.twitter;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

@Configuration
public class TwitterConfiguration
{

    @Value("${twitter.authConsumerKey}")
    private String authConsumerKey;
    @Value("${twitter.authConsumerSecret}")
    private String authConsumerSecret;
    @Value("${twitter.authAccessToken}")
    private String authAccessToken;
    @Value("${twitter.authAccessTokenSecret}")
    private String authAccessTokenSecret;

    @Bean
    public Twitter twitter(@NotNull TwitterFactory twitterFactory)
    {
        return twitterFactory.getInstance();
    }

    @Bean
    public TwitterFactory twitterFactory()
    {
        return new TwitterFactory(new ConfigurationBuilder()
                                          .setDebugEnabled(false)
                                          .setOAuthConsumerKey(authConsumerKey)
                                          .setOAuthConsumerSecret(authConsumerSecret)
                                          .setOAuthAccessToken(authAccessToken)
                                          .setOAuthAccessTokenSecret(authAccessTokenSecret)
                                          .build());
    }
}
